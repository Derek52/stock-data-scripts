library(quantmod)
#setwd("/home/tchalla/Stocks")

source("functions.R")

tiks = c("ALN", "BPMX", "F", "IGC", "UAMY", "CPHI", "AKER", "DPW", "AUY", "GERN", "BUR", "ABIL",
         "AKG", "ALN", "ATNM", "ATOS", "GSAT", "JAGX", "CEI", "OHRP", "TOPS", "CBAK", "ROX", "GEN",
         "MBII", "CATB", "MEIP", "ZNGA", "TENX", "RVP", "APPS", "INSG", "MVIS", "CTRV", "SPEX", "FCEL",
         "VSLR", "MRDN", "MDXG", "JRJR", "IBIO", "HUSA", "TTPH", "APRI", "ACHV") #tickers

fileName = paste0(Sys.Date(), "-Record.txt")

newLine <- function(){
  write("", append=T, fileName)
}

for (ticker in tiks) {
  stock = getSymbols(ticker, auto.assign = F)
  write(ticker, fileName, append=T)
  close = last(Cl(stock), '1 day')
  write(paste0("Last Close: ", close), fileName, append=T)

  timePeriod = '52 weeks'
  write(writeHiLo(close, stock, timePeriod), fileName, append=T)

  timePeriod = '8 weeks'
  write(writeHiLo(close, stock, timePeriod), fileName, append=T)

  timePeriod = '2 weeks'
  write(writeHiLo(close, stock, timePeriod), fileName, append=T)
  newLine()
}
